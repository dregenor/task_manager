#!/usr/bin/env python

from setuptools import setup

VERSION = '0.1'

setup(
    name='task_manager',
    version=VERSION,
    description='stub description',
    author='Artamonov Ivan',
    author_email='dregenor@gmail.com',

    packages=['task_manager'],

    install_requires=[
        'django == 1.9',
        'psycopg2',
        'python-dateutil',
        'Pillow',
        'django-jsonfield',
        'sorl-thumbnail',
        'django-messages-extends',
        'django-contrib-comments',
        'django-allauth',
        'django-oauth-toolkit',
        'django-cors-headers',
        'djangorestframework',
        'markdown',
        'django-rest-auth',
        'django-filter'
    ]
)
