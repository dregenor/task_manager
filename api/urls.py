from django.conf.urls import include, url
from .views import router, image_file

__author__ = 'dreg'


urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^image_file', image_file, name="image_file"),
    url(r'^rest-auth/', include('rest_auth.urls')),
    url(r'^rest-auth/registration/', include('rest_auth.registration.urls')),

]
