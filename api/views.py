import uuid
import os
from rest_framework import routers, viewsets, permissions
from accounts.models import User, WorkGroup
from core.models import Stream, Task, Files
from rest_framework.filters import DjangoFilterBackend, SearchFilter, OrderingFilter
from rest_framework.decorators import list_route, api_view, permission_classes
from rest_framework.response import Response
from django.core.files.storage import FileSystemStorage


from api.serializers import UserSerializer, WorkGroupSerializer, StreamSerializer, TaskSerializer, FilesSerializer

__author__ = 'dreg'

default_filter_backends = (DjangoFilterBackend, SearchFilter, OrderingFilter)


class IsOwner(permissions.BasePermission):
    """
    Custom permission to only allow owners of an object to edit it.
    """

    def has_object_permission(self, request, view, obj):
        return (
            request.user and
            request.user.is_authenticated() and
            (obj == request.user or obj.user == request.user)
        )


class UserViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = User.objects
    serializer_class = UserSerializer
    filter_fields = ("id", "username", "email")
    search_fields = ["username"]
    ordering_fields = ("id", "username")
    filter_backends = default_filter_backends
    permission_classes = (permissions.IsAuthenticated, )

    @list_route()
    def me(self, request, *args, **kwargs):
        user_id = request.user.id
        self.kwargs["pk"] = user_id
        return self.retrieve(request, pk=user_id)


class WorkGroupViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = WorkGroup.objects
    serializer_class = WorkGroupSerializer
    filter_fields = ("id", "members__id", "owner__id")
    search_fields = ["name"]
    ordering_fields = ("id", "name")
    filter_backends = default_filter_backends
    permission_classes = (permissions.IsAuthenticated, )


class StreamsViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Stream.objects
    serializer_class = StreamSerializer
    filter_fields = ("id", "owner__id")
    search_fields = ["owner__id"]
    filter_backends = default_filter_backends
    permission_classes = (permissions.IsAuthenticated, )


class TasksViewSet(viewsets.ModelViewSet):
    queryset = Task.objects
    serializer_class = TaskSerializer
    filter_fields = ("id", "owner__id", "curator__id")
    search_fields = ["title", "owner__id"]
    filter_backends = default_filter_backends
    permission_classes = (permissions.IsAuthenticated, )

# todo implement tasks View
# todo implement streams View
# todo implement sessions View


@api_view(http_method_names=["POST"])
@permission_classes([permissions.IsAuthenticated])
def image_file(request):
    new_file = request.FILES.get('file')
    storage = FileSystemStorage()

    filename, file_extension = os.path.splitext(new_file.name)
    new_name = uuid.uuid4().hex + file_extension

    name = storage.save(name=new_name, content=new_file, max_length=40)
    src_url = storage.url(name)

    files_model = Files.objects.create(url=src_url, path=name, owner=request.user)
    result = FilesSerializer(files_model, context={'request': request}).data
    return Response(result)


"""
routes
"""

router = routers.DefaultRouter()
router.register(r'users', UserViewSet)
router.register(r'work_group', WorkGroupViewSet)
router.register(r'streams', StreamsViewSet)
router.register(r'tasks', TasksViewSet)
