from rest_framework import serializers
from rest_framework.fields import empty
from accounts.models import User, WorkGroup
from core.models import Task, Session, Stream, StreamShareToken, Files

__author__ = 'dreg'


class UserSerializer(serializers.ModelSerializer):
    avatar = serializers.ImageField(required=False, default=empty)

    class Meta:
        model = User
        fields = ('username', 'id', 'email', 'created', 'avatar', 'streams_limit')


class WorkGroupSerializer(serializers.ModelSerializer):
    owner = UserSerializer()
    members = UserSerializer(many=True)

    class Meta:
        model = WorkGroup
        fields = ('name', 'owner', 'members')


class SessionSerializer(serializers.ModelSerializer):
    start = serializers.DateTimeField(required=False)
    executor = UserSerializer()

    class Meta:
        model = Session
        fields = ('start', 'length', 'executor', 'task_id', 'stream_id')


class TaskSerializer(serializers.ModelSerializer):
    owner = UserSerializer()
    curator = UserSerializer()
    # sessions = SessionSerializer(many=True)

    class Meta:
        model = Task
        fields = ('created_at', 'owner', 'curator', 'brief', 'title', 'color')


class StreamShareTokenSerizlizer(serializers.ModelSerializer):
    owner = UserSerializer()

    class Meta:
        model = StreamShareToken
        fields = ('stream_id', 'description', 'token', 'owner')


class StreamSerializer(serializers.ModelSerializer):
    owner = UserSerializer()
    allow_users = UserSerializer(many=True, required=False)
    allow_groups = WorkGroupSerializer(many=True, required=False)
    allow_tokens = StreamShareTokenSerizlizer(many=True, required=False)

    class Meta:
        model = Stream
        fields = ('owner', 'allow_users', 'allow_groups', 'allow_tokens')


class FilesSerializer(serializers.ModelSerializer):
    owner_id = serializers.ReadOnlyField()

    class Meta:
        model = Files
        fields = ['url', 'owner_id']
