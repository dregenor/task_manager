from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    url(r'^api/', include('api.urls')),
    url(r'^admin/', include(admin.site.urls))
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT) + [
    url(r'^', include('core.urls', namespace='core'))
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) \
