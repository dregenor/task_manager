from django.db import models
from django.contrib.auth.models import AbstractUser


class User(AbstractUser):
    def get_full_name(self):
        full_name = '%s %s' % (self.first_name, self.last_name)
        return full_name.strip()

    def get_short_name(self):
        return self.first_name

    created = models.DateTimeField(auto_now_add=True)
    avatar = models.ImageField(blank=True, null=True)
    streams_limit = models.IntegerField(default=1)

    @property
    def current_task(self):
        return None

    def __unicode__(self):
        return "User: %s(%s)" % (self.username, self.email)


class WorkGroup(models.Model):
    name = models.CharField(max_length=300)
    owner = models.ForeignKey("accounts.User", related_name="owner_in_groups")
    members = models.ManyToManyField("accounts.User", related_name="member_in_groups", blank=True)

    def __unicode__(self):
        return "%s(%d) created by %s" % (self.name, self.members.count(), self.owner)
