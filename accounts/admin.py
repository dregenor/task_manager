
__author__ = 'dreg'

from django.contrib import admin
from accounts.models import User, WorkGroup
from django.forms import modelform_factory

classes = [User, WorkGroup]

for cls in classes:
    class ClassAdmin(admin.ModelAdmin):
        form = modelform_factory(cls, fields='__all__')
    admin.site.register(cls, ClassAdmin)

# Register your models here.
