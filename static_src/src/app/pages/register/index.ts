import {Component} from 'angular2/core';
import {pagesPath} from '../../settings';
import {RegisterForm} from '../../components/register-form/index';

@Component({
  selector: 'Register',
  directives: [RegisterForm],
  templateUrl: pagesPath.tpl('register')
})
export class Register {}
