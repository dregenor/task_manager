<div class="user-info">
  <div class="row">
    <div class="col-sm-12">
      <h2>User Info</h2>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-2"><b>Name</b></div>
    <div class="col-sm-4">{{ user.username }}</div>
  </div>
  <div class="row">
    <div class="col-sm-2"><b>Id</b></div>
    <div class="col-sm-4">{{ user.id }}</div>
  </div>
  <div class="row">
    <div class="col-sm-2"><b>Email</b></div>
    <div class="col-sm-4">{{ user.email }}</div>
  </div>
  <div class="row">
    <div class="col-sm-2"><b>Created</b></div>
    <div class="col-sm-4">{{ user.created | date }}</div>
  </div>
  <div class="row">
    <div class="col-sm-2"><b>Avatar</b></div>
    <div class="col-sm-4"><img [src]="user.avatar"></div>
  </div>
  <div class="row">
    <div class="col-sm-2"><b>Streams Limit</b></div>
    <div class="col-sm-4">{{ user.streams_limit }}</div>
  </div>
</div>
<div class="class_groups_info">

  <div class="row">
    <div class="col-sm-12">
      <div class="panel-heading"><h2>Groups Info</h2></div>
      <table class="table">
        <thead>
          <tr>
            <th>#</th>
            <th>Group Name</th>
            <th>Owner Name</th>
            <th>Total Members</th>
          </tr>
        </thead>
        <tbody>
          <tr *ngFor="#group of groups, #i=index">
            <th scope="row">{{ i+1 }}</th>
            <td>{{ group.name }}</td>
            <td>{{ group.owner.username }}</td>
            <td>{{ group.members.length }}</td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
</div>