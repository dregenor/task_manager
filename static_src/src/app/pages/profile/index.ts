import {Component, OnDestroy, Inject} from 'angular2/core';
import {pagesPath} from '../../settings';

import {CORE_DIRECTIVES} from 'angular2/common';

import {Authorization, API} from '../../API/index';
import * as USER from '../../models/user';

@Component({
  selector: 'Profile',
  directives: [CORE_DIRECTIVES],
  templateUrl: pagesPath.tpl('profile'),
})
export class Profile implements OnDestroy {
  user: USER.UserModel;
  groups: Array<USER.GroupModel>;
  unSubscribers: Array<any> = [];
  constructor(
    private auth: Authorization,
    @Inject(API) private api: API
  ) {
    this.unSubscribers.push(
      this.auth.onProfile.subscribe((user:USER.UserModel) => {
        if(user){
          this.user = user;
          this.getGroups(this.user);
        } else {
          this.groups = [];
          this.user = null;
        }
      })
    );
  }

  getGroups(user: USER.UserModel){
    this.api.groups.list({members__id: user.id}).subscribe((groups: Array<USER.GroupModel>)=>{
      this.groups = groups;
    })
  }

  ngOnDestroy() {
    this.unSubscribers.forEach(subscriber => {
      subscriber.unsubscribe();
    });
  }
}