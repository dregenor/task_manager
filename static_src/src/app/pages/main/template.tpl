<div>
  <streams [user]="user"></streams>
  <sessions [user]="user" [free]="true"></sessions>
  <task-form [user]="user"></task-form>
</div>