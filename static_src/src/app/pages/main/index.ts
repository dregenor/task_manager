import {Component, OnDestroy, OnInit} from 'angular2/core';
import {pagesPath} from '../../settings';

import {CORE_DIRECTIVES} from 'angular2/common';

import {Authorization, API} from '../../API/index';
import {UserModel} from '../../models/user';

import {StreamsComponent, SessionsComponent, TaskFormComponent} from '../../components';
import {TaskModel} from "../../models/core";


@Component({
  selector: 'Main',
  directives: [CORE_DIRECTIVES, StreamsComponent, SessionsComponent, TaskFormComponent],
  templateUrl: pagesPath.tpl('main')
})
export class Main implements OnDestroy, OnInit {
  user: UserModel;
  tasks: Array<TaskModel> = [];

  unSubscribers: Array<any> = [];
  constructor( private auth: Authorization, private api: API) {}

  ngOnInit(){
    this.unSubscribers.push(
      this.auth.onProfile.subscribe((user:UserModel) => {
        if(user){ this.user = user; this.getTasks(); }
        else { this.user = null }
      })
    );
  }

  getTasks(){
    this.api.tasks.list({owner__id:this.user.id}).subscribe((tasks:Array<TaskModel>)=>{
      this.tasks = tasks;
    })
  }

  ngOnDestroy() {
    this.unSubscribers.forEach(subscriber => { subscriber.unsubscribe() });
  }
}