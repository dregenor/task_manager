import {Component, Inject, OnInit, OnDestroy} from 'angular2/core';
import {pagesPath} from '../../settings'
import {API, Authorization} from '../../API/index'

@Component({
  selector: 'home',
  templateUrl: pagesPath.tpl('home'),
})
export class Home{}