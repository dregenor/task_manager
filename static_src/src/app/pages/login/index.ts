import {Component} from 'angular2/core';
import {pagesPath} from '../../settings'
import {LoginForm} from '../../components/login-form/index';

@Component({
    selector: 'Login',
    templateUrl: pagesPath.tpl('login'),
    directives: [LoginForm]
})
export class Login {}