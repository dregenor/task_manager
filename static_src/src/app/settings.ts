import {PackageTemplateBase} from './PackageTemplateBase';
declare var APP_PACKAGE_PATH : String;


export var templatePath : PackageTemplateBase =  new PackageTemplateBase(APP_PACKAGE_PATH, ['app']);
export var componentsPath : PackageTemplateBase =  templatePath.sub(['components'], "/template.tpl");
export var pagesPath : PackageTemplateBase =  templatePath.sub(['pages'], "/template.tpl");
