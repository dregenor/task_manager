///<reference path="../../node_modules/ng2-bootstrap/ng2-bootstrap.d.ts"/>
///<reference path="../../typings/browser/definitions/moment/moment.d.ts"/>
///<reference path="../../typings/browser/ambient/es6-shim/es6-shim.d.ts"/>
///<reference path="../../typings/browser/ambient/require/require.d.ts"/>
import {ColorPickerService} from './thirdparty/color-picker/color-picker_service'
import {provide} from "angular2/core";
import {bootstrap} from "angular2/platform/browser";
import {HTTP_PROVIDERS} from "angular2/http";
import {ROUTER_PROVIDERS, APP_BASE_HREF} from "angular2/router";
import {API, Authorization} from "./API/index";


import {Component} from 'angular2/core';
import {ROUTER_DIRECTIVES, RouteConfig} from 'angular2/router';
import {CORE_DIRECTIVES} from 'angular2/common';

import {Header} from './components';
import {PAGES, ROUTING} from './pages';


@Component({
    selector: 'app',
    template: `
      <header></header>
      <div class="container">
        <router-outlet></router-outlet>
      </div>
    `,
    directives: [
      CORE_DIRECTIVES,
      ROUTER_DIRECTIVES,
      PAGES,
      Header
    ]
})
@RouteConfig(ROUTING)
export class AppComponent{}

bootstrap(AppComponent, [
  ROUTER_PROVIDERS,
  HTTP_PROVIDERS,
  provide(APP_BASE_HREF, {useValue: "/"}),
  provide(API, {useClass: API}),
  provide(Authorization, {useClass: Authorization}),
  ColorPickerService
]);
