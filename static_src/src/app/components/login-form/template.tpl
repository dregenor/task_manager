<div>
  <form class="form-horizontal" [ngFormModel]="form" #f="ngForm" (ngSubmit)="doLogin()">
    <div class="form-group"><h3>Authorization</h3></div>

    <field label="Login">
      <input class="form-control" type="text" ngControl="username">
    </field>
    <field label="Password">
      <input class="form-control" type="password" ngControl="password">
    </field>

    <div class="form-group">
      <div class="col-sm-12 text-right">
        <button class="btn btn-primary" type="submit" [disabled]="!f.form.valid">Enter</button>
      </div>
    </div>
    <error [errors]="errMessages"></error>
  </form>
</div>