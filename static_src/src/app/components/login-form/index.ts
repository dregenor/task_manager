import { Inject, Component } from "angular2/core"
import {
  FORM_PROVIDERS,
  FORM_DIRECTIVES,
  CORE_DIRECTIVES,
  ControlGroup,
  FormBuilder,
  Validators
} from "angular2/common"

import {Router} from 'angular2/router'

import {formUtil} from '../../helpers/formUtil'
import {componentsPath} from '../../settings'
import {API, Authorization} from "../../API/index"

import {FormFieldComponent} from '../form.field'
import {FormErrorComponent} from '../form.error'
import {LabelsComponent} from  '../labels.ngform'
import {LabelsValueAccessor} from  '../labels.ngform.accessor'

@Component({
  selector: 'login-form',
  viewBindings: [FORM_PROVIDERS],
  providers: [FormBuilder],
  templateUrl: componentsPath.tpl('login-form'),
  directives: [
    CORE_DIRECTIVES, FORM_DIRECTIVES, FormFieldComponent, FormErrorComponent, LabelsComponent, LabelsValueAccessor
  ]
})
export class LoginForm {
  errMessages: Array<string> = [];
  form: ControlGroup;

  constructor(  
    fb: FormBuilder, 
    private api: API,
    private auth: Authorization,
    private router: Router
  ) {
    this.form = fb.group({
      "username":[""],
      "password":[""] //, Validators.required
    })
  }
  doLogin() {
    this.api.auth
      .login(JSON.stringify(this.form.value))
      .subscribe(
        ( data )=>{
          this.auth.key = data.key;
          this.router.navigate(['/Home']);
        },
        ( err  )=>{
          this.errMessages = [];
          formUtil.displayErrors(err,this.form,(messages)=>{
            this.errMessages = this.errMessages.concat(messages)
          });
        }
      );
  }
}