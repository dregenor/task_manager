import {Component,Input,SimpleChange,OnDestroy, OnInit, OnChanges} from 'angular2/core';
import {UserModel} from '../../models/user'
import {StreamModel} from '../../models/core';
import {Authorization, API} from '../../API/index';

@Component({
  selector: 'streams',
  template: `
    <div> streams for {{ user?.username }} have {{ streams?.length }} </div>
  `
})
export class StreamsComponent implements OnDestroy, OnInit, OnChanges{
  unSubscribers: Array<any> = [];
  streams: Array<StreamModel> = [];

  @Input()
  user:UserModel;

  constructor(private api: API){
    this.streams = [];
  }

  ngOnChanges(changes: {[propName: string]: SimpleChange}){
    if(changes['user'].currentValue){
      this.unSubscribers.push(
        this.api.streams
          .list({owner__id:this.user.id})
          .subscribe((streams: Array<StreamModel>)=>{ this.streams = streams })
      );
    } else {
      this.streams = [];
    }
  }

  ngOnInit(){}

  ngOnDestroy() {
    this.unSubscribers.forEach(subscriber => { subscriber.unsubscribe() });
  }
}
