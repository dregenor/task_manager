import {Component,Input, OnDestroy, OnInit, OnChanges, SimpleChange} from 'angular2/core';
import {UserModel} from '../../models/user';
import {API} from '../../API/index';


@Component({
  selector: 'sessions',
  template: `<div> sessions for {{ user?.username }} </div>`
})
export class SessionsComponent {

  @Input()
  user:UserModel;

  @Input()
  free:boolean;

  constructor(private api: API){}


}
