import {Component, OnDestroy} from 'angular2/core'
import {componentsPath} from '../../settings'
import {API, Authorization} from '../../API/index'

import {ROUTER_DIRECTIVES} from 'angular2/router'
import {CORE_DIRECTIVES} from 'angular2/common'
import {DROPDOWN_DIRECTIVES} from 'ng2-bootstrap/ng2-bootstrap';

@Component({
    selector: 'header',
  templateUrl: componentsPath.tpl('header'),
  directives:[CORE_DIRECTIVES,ROUTER_DIRECTIVES,DROPDOWN_DIRECTIVES]
})
export class Header implements OnDestroy{
  name: String = 'Task Manager';
  version: String = '0.0.1';
  unSubscribers: Array<any> = [];
  user: Object = null;
  isAuth: boolean = false;
  constructor(private auth: Authorization) {
    this.unSubscribers.push(this.auth.onProfile.subscribe((user)=>{ this.user = user }));
    this.unSubscribers.push(this.auth.onAuthorised.subscribe((isAuth)=>{ this.isAuth = isAuth}))
  }
  ngOnDestroy(){
    this.unSubscribers.forEach(subscriber=>{ subscriber.unsubscribe() })
  }
}
