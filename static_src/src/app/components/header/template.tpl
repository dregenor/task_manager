<header class="navbar navbar-static-top">
  <div class="container">
    <div class="navbar-header">
      <a [routerLink]="['/Home']" class="navbar-brand">{{ name }}{{ verison }}</a>
    </div>
    <nav class="collapse navbar-collapse">
      <ul class="nav navbar-nav">
        <li *ngIf="!isAuth"><a [routerLink]="['/Login']">Login</a></li>
        <li *ngIf="!isAuth"><a [routerLink]="['/Register']">Register</a></li>
        <li *ngIf="isAuth" dropdown>
          <a href dropdownToggle class="dropdown-toggle">{{ user?.username }} <span class="caret"></span></a>
          <ul class="dropdown-menu" dropdownMenu>
            <li><a [routerLink]="['/Main']">Main</a></li>
            <li role="separator" class="divider"></li>
            <li><a [routerLink]="['/Profile']">Profile</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="#" (click)="auth.logout()">Logout</a></li>
          </ul>
        </li>
      </ul>
    </nav>
  </div>
</header>
