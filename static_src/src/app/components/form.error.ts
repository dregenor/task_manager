import {Component,Input} from 'angular2/core';

@Component({
  selector: 'error',
  template: `
    <div *ngIf="errors.length>0" class="form-group form-group-sm">
      <div class="alert alert-warning alert-dismissible col-md-10 col-sm-10" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close" (click)="close()"><span aria-hidden="true">&times;</span></button>
        <span *ngFor="#error of errors">{{error}}</span>
      </div>
    </div>
  `
})
export class FormErrorComponent {
  @Input()
  errors:Array<string>;

  close() {
    this.errors = [];
  }
}