import {Component, Input, OnInit} from 'angular2/core';
import {UserModel} from '../../models/user'
import {
  FORM_PROVIDERS,
  FORM_DIRECTIVES,
  CORE_DIRECTIVES,
  ControlGroup,
  FormBuilder,
  Validators,
} from "angular2/common"

import {FormFieldComponent} from '../form.field'
import {FormErrorComponent} from '../form.error'
import {LabelsComponent} from  '../labels.ngform'
import {LabelsValueAccessor} from  '../labels.ngform.accessor'
import {API} from "../../API/index";
import {TYPEAHEAD_DIRECTIVES} from "ng2-bootstrap/ng2-bootstrap";
import {Control} from "angular2/common";
import {ColorPickerDirective} from "../../thirdparty/color-picker/color-picker.directive";
import {componentsPath} from '../../settings'
import {TextBlocksDirective} from '../text-blocks/index'

let directives : any[] = [
  TYPEAHEAD_DIRECTIVES,
  FORM_DIRECTIVES,
  CORE_DIRECTIVES,
  FormFieldComponent, FormErrorComponent, LabelsComponent, LabelsValueAccessor,
  ColorPickerDirective, TextBlocksDirective
];

@Component({
  selector: 'task-form',
  templateUrl: componentsPath.tpl('task-form'),
  providers: [FORM_PROVIDERS],
  directives: directives
})
export class TaskFormComponent implements OnInit {
  @Input()
  user: UserModel;
  curatorSelected: string = "";

  form: ControlGroup;

  constructor(private _fb:FormBuilder, private api: API) {}

  ngOnInit(){
    this.form = this._fb.group({
      curator: new Control(null),
      brief: [`{"blocks":[{"type":"text","data":"task"},{"data":[],"type":"image"}]}`],
      title: [""],
      color: ["#aaffee"]
    });
  }


  private _cache:any;
  private _prevContext:any;

  curatorOnSelect(e:any){
    let curator: Control = <Control>this.form.controls['curator'];
    curator.updateValue(e.item.id, {});
    console.log(this.form.value);
  }

  getUsers(curatorSelected:any):Function{
    if (this._prevContext === curatorSelected) {
      return this._cache;
    }
    this._prevContext = curatorSelected;

    var p:Promise<UserModel[]> = new Promise((resolve:Function) => {
      this.api.users
        .list({search: curatorSelected})
        .subscribe((users:UserModel[])=>{
          resolve(users)
        });
    });


    this._cache = function ():Promise<UserModel[]> {
      return p;
    };
    return this._cache;
  }

  private color: string = "#aaffee";

  onSubmit(){
    // todo call api task create
    console.log(this.form.value)
  }

}
