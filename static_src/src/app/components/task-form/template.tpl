<div class="panel panel-default">
  <div class="panel-heading">Add new task</div>
  <div class="panel-body">
    <form class="form-horizontal"  [ngFormModel]="form" #f="ngForm" (ngSubmit)="onSubmit()">
      <field label="title">
        <input class="form-control" type="text" ngControl="title">
      </field>
      <field label="color">
        <input
          style="width: 80px"
          class="form-control"
          type="text"
          [(colorPicker)]="color"
          [value]="color"
          ngControl="color"
          [style.background]="color"
        >
      </field>
      <field label="brief">
        <textarea ngControl="brief" text-block></textarea>
      </field>
      <div class="row">
        <div class="col-sm-12 text-right">
          <button class="btn btn-primary" type="submit" [disabled]="!f.form.valid">add task</button>
        </div>
      </div>
    </form>
  </div>
</div>