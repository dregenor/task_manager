import {Directive,OnDestroy, OnInit, ElementRef, HostListener, DynamicComponentLoader, ComponentRef} from 'angular2/core';
import {TextBlocksEditor, TextBlocks} from "./editor/index";

@Directive({
  selector: 'textarea[text-block]'
})
export class TextBlocksDirective implements OnInit, OnDestroy{

  constructor(private el:ElementRef,private dcl: DynamicComponentLoader){}

  renderer:ComponentRef;

  // todo use rxjs to stream data
  data:TextBlocks;

  ngOnInit(){
    this.el.nativeElement.hidden = true;
    this.importFromTextarea();
    this.dcl
      .loadNextToLocation(TextBlocksEditor, this.el)
      .then((renderer:ComponentRef)=>{
        this.renderer = renderer;
        this.renderer.instance.data = this.data;
      });
  }

  ngOnDestroy(){
    this.renderer.dispose();
  }

  @HostListener('change')
  onChange() {
    this.importFromTextarea()
  }

  importFromTextarea(){
    let
      d: TextBlocks = {
        blocks:[]
      };
    try{ d  = JSON.parse(this.el.nativeElement.value) } catch(e) {}
    this.data = d;

    if (this.renderer){
      this.renderer.instance.data = this.data;
    }
  }

}