import {
  Component, Input, ElementRef, OnChanges, OnDestroy, SimpleChange, DynamicComponentLoader,
  ComponentRef
} from 'angular2/core';

import { CORE_DIRECTIVES } from "angular2/common"
import {SimpleTextBlock} from "./blocks/simpletextblock";
import {ImageBlock} from "./blocks/imageblock";


export interface TextBlock {
  data: string
  type: string
}

export interface TextBlocks {
  blocks: TextBlock[]
}


@Component({
  selector: '[text-block-editor-item]',
  template: `<div #child></div>`,
  directives: [
    CORE_DIRECTIVES
  ]
})
export class TextBlocksEditorItem implements OnChanges, OnDestroy{
  @Input() block: TextBlock;
  constructor(private el:ElementRef, private dcl: DynamicComponentLoader){}

  ngOnChanges(changes: {[propName: string]: SimpleChange}){
    var c: SimpleChange = changes['block'];
    if(c && c.currentValue.type !== c.previousValue.type) {
      this.reloadBlock()
    }
  }

  private _editItem: ComponentRef;

  reloadBlock(){
    var blockClass: any = null;

    if (this._editItem){ this._editItem.dispose() }

    if(this.block.type == 'text'){ blockClass = SimpleTextBlock }
    if(this.block.type == 'image'){ blockClass = ImageBlock }

    this.dcl
        .loadIntoLocation(blockClass, this.el, 'child')
        .then((el:ComponentRef)=>{
          this._editItem = el;
          this._editItem.instance.data = this.block.data;
          this._editItem.instance.isEdit = true;
        })
  }

  ngOnDestroy(){
    if(this._editItem){ this._editItem.dispose() }
    this._editItem = null;
  }

}


@Component({
  selector: 'text-blocks-renderer',
  template: '<div *ngFor="#block of data.blocks" text-block-editor-item [block]="block"></div>',
  directives: [ CORE_DIRECTIVES, TextBlocksEditorItem ]
})
export class TextBlocksEditor{
  @Input() data:TextBlocks;
}

