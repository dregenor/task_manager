import {Component, Input, ElementRef, OnInit, HostListener, OnChanges, SimpleChange} from "angular2/core";

declare var MediumEditor: any;

@Component({
  selector:'text-block-renderer',
  template: `
    <div class="block block-simple-text">
      <div [hidden]="!isEdit">
        <textarea [(ngModel)]="data"></textarea>
      </div>
      <div [hidden]="isEdit" [innerHTML]="data"></div>
    </div>
  `
})
export class SimpleTextBlock implements OnInit, OnChanges{
  @Input()
  data: string;

  @Input()
  isEdit: boolean = false;

  editor: any = null;
  constructor(private el: ElementRef){}

  ngOnInit(){

    this.editor = new MediumEditor(this.el.nativeElement.querySelectorAll('textarea'),{
      autoLink: true,
      toolbar: {
        buttons: ['bold', 'italic', 'underline', 'anchor', 'h2', 'h3', 'quote','orderedlist','unorderedlist'],
      }
    });
    this.editor.setContent(this.data);
  }

  ngOnChanges(changes: {[propName: string]: SimpleChange}){
    var c: SimpleChange = changes['data'];
    if(c && c.currentValue) {
      console.log('c.currentValue',c.currentValue);
      this.editor && this.editor.setContent(c.currentValue)
    }
  }

}

