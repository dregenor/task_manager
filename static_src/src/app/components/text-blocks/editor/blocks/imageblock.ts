import {Component, OnInit, ElementRef, Input, OnDestroy} from "angular2/core";
import {FILE_UPLOAD_DIRECTIVES,FileUploader} from '../../../../thirdparty/file-upload/index';
import {NgClass, NgStyle, CORE_DIRECTIVES, FORM_DIRECTIVES} from "angular2/common";
import {Authorization, BaseApi} from "../../../../API/index";

interface ImageBlockData {
  url:string;
  owner_id: number;
}

const URL = "/api/image_file";

@Component({
  selector:'image-block',
  template: `<div class="block block-image">
    <div class="previews">
      <img *ngFor="#ib of data" [src]="ib.url" class="thumbnail">
    </div>
    <div class="uploader">
      <input type="file" ng2-file-select [uploader]="uploader" multiple  />
    </div>
  </div>`,
  directives: [FILE_UPLOAD_DIRECTIVES, NgClass, NgStyle, CORE_DIRECTIVES, FORM_DIRECTIVES]
})
export class ImageBlock implements OnInit, OnDestroy{

  constructor(private el: ElementRef, private auth:Authorization){}

  @Input()
  data: ImageBlockData[];

  @Input()
  isEdit: boolean = false;

  unSubscribers: Array<any> = [];

  private uploader:FileUploader = new FileUploader({url: URL});

  ngOnInit(){
    this.unSubscribers.push(
      this.auth.onAuthorised.subscribe(()=>{
        this.uploader.authToken = `Token ${this.auth.key}`
      })
    );
    this.uploader.autoUpload = true;
    this.uploader.queueLimit = 999;
    this.uploader.removeAfterUpload = true;
    this.uploader.onCompleteItem = this.onCompleteItem.bind(this);

    this.uploader.onBeforeUploadItem = this.onBeforeUploadItem.bind(this)
  }

  ngOnDestroy(){
    this.unSubscribers.forEach(subscriber => { subscriber.unsubscribe() });
  }

  onBeforeUploadItem(item:any){
    var csrf = BaseApi._getCsrf();
    if(csrf) item.headers.push({name:"X-CSRFToken",value:csrf});
  }

  onCompleteItem(item:any, response:any, status:any, headers:any){
    console.log(response);
    // todo move this to file uploader
    try{
      response = JSON.parse(response);
      this.data.push(<ImageBlockData>response);
    } catch (e){}
  }
}