import {
  FORM_PROVIDERS,
  FORM_DIRECTIVES,
  ControlGroup,
  FormBuilder,
  Validators,
} from "angular2/common"
import{ Component, Inject } from "angular2/core"
import {Http, Headers, HTTP_BINDINGS} from 'angular2/http';
import {Router} from 'angular2/router'

import {formUtil} from '../../helpers/formUtil'
import {componentsPath} from '../../settings'
import {API, Authorization} from "../../API/index"

import {FormFieldComponent} from '../form.field'
import {FormErrorComponent} from '../form.error'
import {LabelsComponent} from  '../labels.ngform'
import {LabelsValueAccessor} from  '../labels.ngform.accessor'


@Component({
  selector: 'register-form',
  viewBindings: [FORM_PROVIDERS],

  providers : [FormBuilder],
  directives: [
    FORM_DIRECTIVES,
    FormFieldComponent,
    FormErrorComponent,
    LabelsComponent,
    LabelsValueAccessor
  ],
  templateUrl: componentsPath.tpl('register-form')
})
export class RegisterForm {
  registerForm: ControlGroup;
  errMessages: Array<string> = [];

  constructor( 
    fb: FormBuilder,
    private api: API,
    private auth: Authorization,
    private router: Router 
  ) {
    this.registerForm = fb.group({
      username: ["", Validators.required],
      email: ["", Validators.required],
      password1: ["", Validators.required],
      password2: ["", Validators.required]
    });
  }

  doRegister(){
    this.api.auth
      .register(JSON.stringify(this.registerForm.value))
      .subscribe(
        ( data )=>{
          this.auth.key = data.key;
          this.router.navigate(['/Home']);
        },
        ( err  )=>{
          this.errMessages = [];
          formUtil.displayErrors(err,this.registerForm,(messages)=>{
            this.errMessages = this.errMessages.concat(messages)
          });
        }
      );
  }
}