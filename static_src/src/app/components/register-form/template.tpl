<form class="form-horizontal" [ngFormModel]="registerForm" (submit)="doRegister($event)">
  <div class="form-group"><h3>Registration</h3></div>
  <field label="Login">
    <input type="text" class="form-control" ngControl="username" >
  </field>
  <field label="Email">
    <input type="email" class="form-control" ngControl="email" >
  </field>
  <field label="Password">
    <input type="password" class="form-control" ngControl="password1" >
  </field>
  <field label="Confirm password">
    <input type="password" class="form-control" ngControl="password2" >
  </field>
  <error [errors]="errMessages"></error>
  <div class="form-group">
    <div class="col-sm-12 text-right">
      <button class="btn btn-primary" type="submit">Register</button>
    </div>
  </div>
</form>