export class PackageTemplateBase{
    path : String = '';
    extension : String = '.tpl';
    parts : String[] = [];
    constructor( basePath : String, parts: String[] = [], extension : String = '.tpl' ){
        this.path = basePath;
        this.parts = parts;
        this.extension = extension;
    }
    tpl( template : String, extension? : String ) : string{
        var pth = [this.path].concat(this.parts).concat([template]);
        return pth.join('/') + (extension || this.extension)
    }
    sub(parts : String[], extension? : String  ) : PackageTemplateBase {
        return new PackageTemplateBase( this.path, this.parts.concat(parts), extension )
    }
}
