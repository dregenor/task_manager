import {
  UserApiResponse,
  GroupApiResponse,
  UserModel,
  GroupModel
} from "./user"

export interface StreamApiResponse {
  id: number;
  owner: UserApiResponse;
  allow_users: Array<UserApiResponse>;
  allow_groups: Array<GroupApiResponse>;
}

export class StreamModel {
  id: number;
  owner: UserModel;
  allow_users: Array<UserModel>;
  allow_groups: Array<GroupModel>;

  constructor(obj: StreamApiResponse){
    this.id = obj.id;
    this.owner = new UserModel(obj.owner);
    this.allow_users = obj.allow_users.map((obj)=>{ return new UserModel(obj) });
    this.allow_groups = obj.allow_groups.map((obj)=>{ return new GroupModel(obj) });
  }
}

export interface TaskApiResponse {
  id: number;
  created_at: string;
  owner: UserApiResponse;
  curator: UserApiResponse;
  brief: any;
  title: string;
  color: string;
}

export class TaskModel {
  id: number;
  created_at: Date;
  owner: UserModel;
  curator: UserModel;
  brief: any;
  title: string;
  color: string;

  constructor(obj: TaskApiResponse){
    this.id = obj.id;
    this.created_at = new Date(Date.parse(obj.created_at));
    this.owner = new UserModel(obj.owner);
    this.curator = new UserModel(obj.curator);
    this.brief = obj.brief;
    this.title = obj.title;
    this.color = obj.color;
  }
}


