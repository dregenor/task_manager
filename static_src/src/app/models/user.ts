
export interface UserApiResponse {
  username: string;
  id: number;
  email: string;
  created: string;
  avatar: string;
  streams_limit: number;
}

export class UserModel {
  username: string;
  id: number;
  email: string;
  created: Date;
  avatar: string;
  streams_limit: number;

  constructor(obj: UserApiResponse){
    this.username = obj.username;
    this.id = obj.id;
    this.email = obj.email;
    this.created = new Date(Date.parse(obj.created));
    this.avatar =  obj.avatar;
    this.streams_limit = obj.streams_limit;
  }
}


export interface GroupApiResponse {
  name: string;
  owner: UserApiResponse;
  members: Array<UserApiResponse>;
}

export class GroupModel {
  name: string;
  owner: UserModel;
  members: Array<UserModel>;

  constructor(obj: GroupApiResponse ){
    this.name = obj.name;
    this.owner = new UserModel(obj.owner);
    this.members = obj.members.map((member: UserApiResponse )=>{return new UserModel(member)})
  }
}
