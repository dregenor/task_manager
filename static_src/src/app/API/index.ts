import {Injectable, Inject} from "angular2/core";
import {
  Http,
  Headers,
  URLSearchParams,
  Response,
  Request,
  RequestMethod
} from "angular2/http";

import {jsCookie} from "../helpers/js-cookie";
import {BehaviorSubject, Observable} from "rxjs/Rx";

import * as user from "../models/user"
import * as core from "../models/core"

declare var BASE_URL: string;

function standartBehavior(request: Observable<Response>) {
  return  request.map(res => res.json()).catch(res => { return Observable.throw(res.json()) });
}

export class BaseApi {
  baseUrl: string = BASE_URL;
  http: Http;

  constructor(http: Http) {
    this.http = http;
  }

  static _getAuth(): string {
    return  `Token ${Authorization.instance.key}`;
  }

  static _getCsrf(): string {
    return jsCookie.getCookie("csrftoken");
  }

  _getHeaders(obj: any = {}, auth: Boolean = false): Headers {
    let headers = new Headers();
    let csrf: string = BaseApi._getCsrf();

    if (csrf){ headers.append("X-CSRFToken", csrf) }
    if (auth) { headers.append("Authorization", BaseApi._getAuth()) }

    headers.append("Content-Type", "application/json");
    Object.keys(obj).forEach((key: string) => {
      headers.append(key,  obj[key] )
    });
    return headers;
  }
}

function makeSearch(obj: any): URLSearchParams {
  let keys: Array<string> = Object.keys(obj);
  let result = new URLSearchParams;
  if(keys.length>0){
    keys.forEach((key)=>{
      result.append(key, obj[key]);
    })
  }
  return result;
}

class RestApi extends BaseApi {
  http: Http;
  context: string = "";

  _getUrl(id?: any) {
    if (id) { return [this.baseUrl, this.context, id, ""].join("/"); }
    else { return [this.baseUrl, this.context, ""].join("/"); }
  }
  _transformResponse(val:any){
    return val
  }

  public adaptData(data: Object):string {
    return JSON.stringify(data);
  }

  one(id:string | number) {

    let reqOptions = {
      url: this._getUrl(id),
      method: RequestMethod.Get,
      headers: this._getHeaders()
    };

    return standartBehavior(
      this.http.request( new Request(reqOptions) )
    ).map((value: any)=>{ return this._transformResponse(value) })
  }

  list(search?: Object) {

    let reqOptions = {
      url: this._getUrl(),
      method: RequestMethod.Get,
      search: makeSearch(search),
      headers: this._getHeaders()
    };
    let req = new Request(reqOptions);
    return standartBehavior(
      this.http.request(req)
    ).map((values: Array<any>)=>{ return values.map(this._transformResponse.bind(this)) })
  }

  create(data: Object) {
    let reqOptions = {
      url: this._getUrl(),
      method: RequestMethod.Post,
      body: this.adaptData(data),
      headers: this._getHeaders()
    };

    return standartBehavior(
      this.http.request(new Request(reqOptions))
    ).map((values: Array<any>)=>{ return values.map(this._transformResponse.bind(this)) });
  }

  update(id: string | number, data:Object) {
    let reqOptions = {
      url: this._getUrl(id),
      method: RequestMethod.Patch,
      body: this.adaptData(data),
      headers: this._getHeaders()
    };

    return standartBehavior(
      this.http.request(new Request(reqOptions))
    ).map((values: Array<any>)=>{ return values.map(this._transformResponse.bind(this)) });
  }
}

class UserApi extends RestApi {
  context = 'users';

  _transformResponse(response: user.UserApiResponse){
    return new user.UserModel(response);
  }

  me() {
    return this.one('me')
  }
}

class WorkGroupApi extends RestApi {
  context = 'work_group';

  _transformResponse(response: user.GroupApiResponse){
    return new user.GroupModel(response);
  }
}

class StreamsApi extends RestApi {
  context = 'streams';

  _transformResponse(response: core.StreamApiResponse){
    return new core.StreamModel(response);
  }
}

class TasksApi extends RestApi {
  context = 'tasks';

  _transformResponse(response: core.TaskApiResponse){
    return new core.TaskModel(response);
  }
}

class AuthApi extends BaseApi {
  context:string = 'rest-auth';

  _getUrl(ep: Object) {
    return [this.baseUrl, this.context, ep, ''].join('/')
  }

  register(data:string) {
    return standartBehavior(this.http.post(
      this._getUrl('registration'),
      data,
      {headers: this._getHeaders()}
    ))
  }

  login(data:string) {
    return standartBehavior(this.http.post(
      this._getUrl('login'),
      data,
      {headers: this._getHeaders()}
    ))
  }
}

@Injectable()
export class API {
  users:UserApi;
  auth:AuthApi;
  groups:WorkGroupApi;
  streams:StreamsApi;
  tasks:TasksApi;

  constructor(@Inject(Http) http:Http) {
    this.users = new UserApi(http);
    this.auth = new AuthApi(http);
    this.groups = new WorkGroupApi(http);
    this.streams = new StreamsApi(http);
    this.tasks = new TasksApi(http);
  }
}

@Injectable()
export class Authorization {

  static instance: Authorization;
  _AuthKey:string = '';

  onAuthorised:BehaviorSubject<boolean>;
  onProfile:BehaviorSubject<Object>;

  constructor(@Inject(API) private api: API) {
    this.onProfile = new BehaviorSubject<boolean>(null);
    this.onAuthorised =  new BehaviorSubject<boolean>(false);
    this.onAuthorised.subscribe((isAuth)=>{
      if(isAuth){ 
        this.api.users.me().subscribe((UserData)=>{
          this.onProfile.next(UserData)
        },()=>{
          localStorage.removeItem('auth_key');
          this.key = null;
        })
      } else {
        this.onProfile.next(null)
      }
    });

    Authorization.instance = this;

    if (localStorage['auth_key']){ this.key = localStorage['auth_key']}
  }

  logout(){
    this.key = void 0
  }

  get key():string {
    return this._AuthKey
  }

  set key(key:string) {
    this._AuthKey = key;
    if (key && !localStorage['auth_key']){ localStorage['auth_key'] = key}
    this.onAuthorised.next(!!key);
  }
}

