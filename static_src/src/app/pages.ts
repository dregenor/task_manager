import {Home} from './pages/home/index';
import {Login} from './pages/login/index';
import {Register} from './pages/Register/index';
import {Profile} from './pages/Profile/index';
import {Main} from './pages/Main/index';

import {Route, Redirect} from 'angular2/router';
import {Type} from "angular2/core";


export const PAGES: Type[] = [Home, Login, Register, Profile, Main];

export var ROUTING = [
  new Redirect({ path: '/', redirectTo:['/Home']}),
  new Route({ path: '/home',      component: Home     , name: 'Home'}),
  new Route({ path: '/main',      component: Main     , name: 'Main'}),
  new Route({ path: '/login',     component: Login    , name: 'Login'}),
  new Route({ path: '/register',  component: Register , name: 'Register'}),
  new Route({ path: '/profile',   component: Profile  , name: 'Profile'})
]