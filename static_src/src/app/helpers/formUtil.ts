import { ControlGroup } from "angular2/common";

interface OutFunc {
  (message: Array<string>): void;
}

interface ApiError {
    [key: string]: Array<string>;
}

export var formUtil = {
  displayErrors: function (error: ApiError, targetForm: ControlGroup, nonFieldsErrorOut: OutFunc): void {
    if (error) {
      for (let key in error){
        if (!error.hasOwnProperty(key)) continue; // skip non main properties

        let messages: Array<string> = error[key];

        if(targetForm.controls[key]){
          targetForm.controls[key].setErrors({ "remote": JSON.stringify(messages)}, { emitEvent: true });
        } else {
          nonFieldsErrorOut(messages);
        }
      }
    } else {
      nonFieldsErrorOut(['unknown error']);
    }
  }
};