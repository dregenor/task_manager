interface CookieAttribute {
    expires?: Date;
    expiresDays?: number;
    path?: string;
    domain?: string;
    secure?: boolean;
}

function extend (...arg: CookieAttribute[]): CookieAttribute {
    let result: CookieAttribute = {};
    for (let i = 0; i < arg.length; i++) {
      let attributes: CookieAttribute = arg[ i ];

      if (attributes.hasOwnProperty('expires')) {
        result.expires = attributes.expires;
      }
      if (attributes.hasOwnProperty('expiresDays')) {
        result.expiresDays = attributes.expiresDays;
      }
      if (attributes.hasOwnProperty('path')) {
        result.path = attributes.path;
      }
      if (attributes.hasOwnProperty('domain')) {
        result.domain = attributes.domain;
      }
      if (attributes.hasOwnProperty('secure')) {
        result.secure = attributes.secure;
      }
    }
    return result;
}

declare function escape( str: string): string

class JsCooke {
    defaults: Object = {};
    setCookie(key: string, value: string, attributes?: CookieAttribute): string {
        attributes = extend({ path: '/' }, this.defaults, attributes);

        if (typeof attributes.expiresDays === 'number') {
            let expires = new Date();
            expires.setMilliseconds(expires.getMilliseconds() + attributes.expiresDays * 864e+5);
            attributes.expires = expires;
        }

        value = encodeURIComponent(value)
                .replace(/%(23|24|26|2B|3A|3C|3E|3D|2F|3F|40|5B|5D|5E|60|7B|7D|7C)/g, decodeURIComponent);


        key = encodeURIComponent(String(key));
        key = key.replace(/%(23|24|26|2B|5E|60|7C)/g, decodeURIComponent);
        key = key.replace(/[\(\)]/g, escape);

        return (document.cookie = [
            key, '=', value,
            attributes.expires && '; expires=' + attributes.expires.toUTCString(), // use expires attribute, max-age is not supported by IE
            attributes.path    && '; path=' + attributes.path,
            attributes.domain  && '; domain=' + attributes.domain,
            attributes.secure ? '; secure' : ""
        ].join(''));
    }
    getCookie(key: string): string {
        let result: string,
            cookies = document.cookie ? document.cookie.split('; ') : [],
            rdecode = /(%[0-9A-Z]{2})+/g;

        for (let i = 0; i < cookies.length; i++) {
            let parts = cookies[i].split('='),
                name = parts[0].replace(rdecode, decodeURIComponent),
                cookie = parts.slice(1).join('=');

            if (cookie.charAt(0) === '"') { cookie = cookie.slice(1, -1) }

            try {
                cookie = cookie.replace(rdecode, decodeURIComponent);
                if (key === name) { result = cookie; break }
            } catch (e) {}
        }
        return result;
    }
    remove(key: string): void {
        this.setCookie(key, '', extend({}, { expiresDays: -1 }));
    }
}


export var jsCookie : JsCooke = new JsCooke();