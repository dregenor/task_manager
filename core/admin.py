from django.contrib import admin
from core.models import Task, Session, Stream, StreamShareToken
from django.forms import modelform_factory

classes = [Task, Session, Stream, StreamShareToken]

for cls in classes:
    class ClassAdmin(admin.ModelAdmin):
        form = modelform_factory(cls, fields='__all__')
    admin.site.register(cls, ClassAdmin)

# Register your models here.
