
from django.db import models
from django.db.models.signals import post_save
from jsonfield import JSONField
from accounts.models import User

import uuid


class Task(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    owner = models.ForeignKey("accounts.User", related_name="created_tasks")
    curator = models.ForeignKey("accounts.User", related_name="curated_tasks")
    brief = JSONField(default="{}")
    title = models.CharField(max_length=300)
    color = models.CharField(max_length=7)
    # todo add deadline_at  - julia request


class Session(models.Model):
    start = models.DateTimeField(null=True, blank=True)
    length = models.IntegerField(null=True, blank=True)
    executor = models.ForeignKey("accounts.User", related_name="sessions")
    task = models.ForeignKey("core.Task", related_name="sessions")
    stream = models.ForeignKey("core.Stream", related_name="sessions")


class Stream(models.Model):
    owner = models.ForeignKey("accounts.User")
    allow_users = models.ManyToManyField("accounts.User", related_name="allowed_streams")
    allow_groups = models.ManyToManyField("accounts.WorkGroup", related_name="allowed_streams")


class StreamShareToken(models.Model):
    owner = models.ForeignKey("accounts.User")
    stream = models.ForeignKey("accounts.User", related_name="allow_tokens")
    description = models.CharField(max_length=200, null=True, blank=True)
    token = models.CharField(max_length=100, blank=True, unique=True, default=uuid.uuid4)


class Files(models.Model):
    url = models.CharField(max_length=100, blank=True, null=True)
    path = models.CharField(max_length=100, blank=True, null=True)
    owner = models.ForeignKey("accounts.User")

# automation
def auto_create_stream(sender, **kwargs):
    if "created" in kwargs and "instance" in kwargs:
        instance = kwargs["instance"]
        if not Stream.objects.filter(owner=instance).exists():
            stream = Stream.objects.create(owner=instance)
            stream.save()

post_save.connect(auto_create_stream, sender=User)
