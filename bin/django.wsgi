#!/opt/local/Library/Frameworks/Python.framework/Versions/2.7/Resources/Python.app/Contents/MacOS/Python


import sys
sys.path[0:0] = [
  '/Users/dregenor/develop/task_manager',
  '/Users/dregenor/develop/task_manager/eggs/django_contrib_comments-1.6.2-py2.7.egg',
  '/Users/dregenor/develop/task_manager/eggs/djangorecipe-2.1.2-py2.7.egg',
  '/Users/dregenor/develop/task_manager/eggs/Django-1.9-py2.7.egg',
  '/Users/dregenor/develop/task_manager/eggs/zc.recipe.egg-2.0.3-py2.7.egg',
  '/Users/dregenor/develop/task_manager/eggs/zc.buildout-2.5.0-py2.7.egg',
  '/Users/dregenor/develop/task_manager/eggs/django_filter-0.11.0-py2.7.egg',
  '/Users/dregenor/develop/task_manager/eggs/django_rest_auth-0.7.0-py2.7.egg',
  '/Users/dregenor/develop/task_manager/eggs/Markdown-2.6.5-py2.7.egg',
  '/Users/dregenor/develop/task_manager/eggs/djangorestframework-3.3.2-py2.7.egg',
  '/Users/dregenor/develop/task_manager/eggs/django_cors_headers-1.1.0-py2.7.egg',
  '/Users/dregenor/develop/task_manager/eggs/django_oauth_toolkit-0.10.0-py2.7.egg',
  '/Users/dregenor/develop/task_manager/eggs/django_allauth-0.24.1-py2.7.egg',
  '/Users/dregenor/develop/task_manager/eggs/django_messages_extends-0.5-py2.7.egg',
  '/Users/dregenor/develop/task_manager/eggs/sorl_thumbnail-12.3-py2.7.egg',
  '/Users/dregenor/develop/task_manager/eggs/django_jsonfield-0.9.19-py2.7.egg',
  '/Users/dregenor/develop/task_manager/eggs/Pillow-3.1.1-py2.7-macosx-10.10-x86_64.egg',
  '/Users/dregenor/develop/task_manager/eggs/python_dateutil-2.4.2-py2.7.egg',
  '/Users/dregenor/develop/task_manager/eggs/psycopg2-2.6.1-py2.7-macosx-10.10-x86_64.egg',
  '/Users/dregenor/develop/task_manager/eggs/setuptools-20.1.1-py2.7.egg',
  '/Users/dregenor/develop/task_manager/eggs/six-1.10.0-py2.7.egg',
  '/Users/dregenor/develop/task_manager/eggs/oauthlib-1.0.3-py2.7.egg',
  '/Users/dregenor/develop/task_manager/eggs/django_braces-1.8.1-py2.7.egg',
  '/Users/dregenor/develop/task_manager/eggs/requests-2.9.1-py2.7.egg',
  '/Users/dregenor/develop/task_manager/eggs/requests_oauthlib-0.6.1-py2.7.egg',
  '/Users/dregenor/develop/task_manager/eggs/python_openid-2.2.5-py2.7.egg',
  ]

import djangorecipe.binscripts

application = djangorecipe.binscripts.wsgi('task_manager.settings', logfile='')
